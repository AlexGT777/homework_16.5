﻿
#include <iostream>
#include <time.h>


int main()
{
    const int N = 5;
    int array[N][N];                        // Задаем имя массива, кол-во строк - N, и кол-во столбцов - N, в нем
    
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    std::cout << "1) The Array: " << '\n' << '\n';

    
    for (int i = 0; i < N; i++)             // переключение по строчкам массива array
    {
        for (int j = 0; j < N; j++)         // переключение по столбцам массива array
        {
            array[i][j] = i + j;            // Согласно Заданию заполняем массив так, чтобы элемент с индексами i и j был равен i + j
            
            std::cout << array[i][j];       // Согласно Заданию Выводим в консоль массив array
        }
        
        std::cout << '\n';
    }
       
    std::cout << '\n';

    int numberOfLine = buf.tm_mday % N;     // вычисление остатка деления текущего числа календаря на N

    std::cout << "2) The number of the line in array is - " << numberOfLine << std::endl; // Выводим номер строки. 
    
    int sum = 0;                            // задаем переменную Сумма

    for (int i = numberOfLine; i <= numberOfLine; i++) // переключение на строчку равную значению numberOfLine
    {
       for (int j = 0; j < N; j++)         // переключение по столбцам массива
       {
            sum += array[i][j];            // вычисляем сумму элементов в строчке массива
                
       }
       std::cout << '\n' << "3) The sum of the line elements is - " << sum << '\n'; //выводим на экран сумму элементов в строчке массива
    }
}
